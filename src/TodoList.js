import React, { useState, useEffect } from 'react';

function TodoList() {
    // Create a state variable to hold the list of todos
    const [todos, setTodos] = useState([]);

    // Function to fetch todos from the Node.js API
    const fetchTodos = async () => {
        const response = await fetch('http://localhost:8080/todo');
        const data = await response.json();
        console.log("data",data)
        setTodos(data);
    }

    // Use the useEffect hook to fetch the todos when the component mounts
    useEffect(() => {
        fetchTodos();
    }, []);

    // Function to add a new todo
    const addTodo = async (todo) => {
        await fetch('http://localhost:8080/todo', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(todo)
        });
        fetchTodos();
    }

    // Function to delete a todo
    const deleteTodo = async (id) => {
        await fetch(`http://localhost:8080/todo/${id}`, {
            method: 'DELETE'
        });
        fetchTodos();
    }

    return (
        <div>
            <h1>Todo List</h1>
            <form
                onSubmit={async (e) => {
                    e.preventDefault();
                    await addTodo({
                        task: e.target.task.value,
                        date: e.target.date.value
                    });
                    e.target.task.value = '';
                    e.target.date.value = '';
                }}
            >
                <label>
                    Task:
                    <input name="task" type="text" />
                </label>
                <label>
                    Due Date:
                    <input name="date" type="date" />
                </label>
                <button type="submit">Add Todo</button>
            </form>
            <ul>
                {todos.length && todos.map((todo) => (
                    <li key={todo._id}>
                        {todo.task} - {todo.date}
                        <button onClick={() => deleteTodo(todo._id)}>Delete</button>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default TodoList
